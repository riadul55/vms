<?php

namespace App\Http\Controllers;

use App\Account;
use App\Member;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $message = null;
        $members = Member::where('user_id', $id)->get();
        $date = Carbon::now();

        $dataList = [];
        $paid = 0;
        $non_paid = 0;
        for ($i=0;$i<count($members);$i++){
            $account = Account::where("member_id", $members[$i]->id)->whereMonth('created_at', $date->month)->first();
//            $data = [
//                'member' => $members[$i],
//                'account' => $account
//            ];
            if ($account){
                $paid++;
            } else {
                $non_paid++;
            }

            $dataList[$i] = [
                'id' => $members[$i]->id,
                'user_id' => $members[$i]->user_id,
                'name' => $members[$i]->name,
                'phone' => $members[$i]->phone,
                'grand_total' => isset($account) ? $account->grand_total : 0,
                'due_bill' => isset($account) ? $account->due_bill : 0,
                'status' => isset($account) ? 1 : 0
            ];
        }

        if ($dataList){
            $message = "Members found";
        } else {
            $message = "Members not found";
        }

        $response = [
            'massage' => $message,
            'token' => null,
            'data' => $dataList,
            'paid' => $paid,
            'non_paid' => $non_paid
        ];

        return response($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $message = null;
        $member = null;
        $token = null;

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();
        } else {
            $member = new Member();
            $member->user_id = $request->get('user_id');
            $member->name = $request->get('name');
            $member->phone = $request->get('phone');
            $member->house_bill = $request->get('house_bill');
            $member->water_bill = $request->get('water_bill');
            $member->gas_bill = $request->get('gas_bill');
            $member->electric_per_unit = $request->get('electric_per_unit');
            $member->description = $request->get('description');
            $member->save();
            $message = "Member created successfully";
        }

        $response = [
            'massage' => $message,
            'token' => $token,
            'data' => $member,
        ];
        return response($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = null;

        $date = Carbon::now();
        $member = Member::find($id);
        $account = Account::where("member_id", $member->id)->first();
//        $account = Account::where("member_id", $member->id)->whereMonth('created_at', $date->month)->first();

        if ($member != null){
            $message = "Member found";
        } else {
            $message = "Member not found";
        }

        $dataList = [
            'member' => $member,
            'account' => $account
        ];

        $response = [
            'massage' => $message,
            'token' => null,
            'data' => $dataList,
        ];

        return response($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = null;
        $member = Member::find($id);

        if ($member != null){
            $message = "Member found";
        } else {
            $message = "Member not found";
        }

        $response = [
            'massage' => $message,
            'token' => null,
            'data' => $member,
        ];

        return response($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = null;
        $member = Member::find($id);
        if ($member != null){
            $member->user_id = $request->get('user_id');
            $member->name = $request->get('name');
            $member->phone = $request->get('phone');
            $member->house_bill = $request->get('house_bill');
            $member->water_bill = $request->get('water_bill');
            $member->gas_bill = $request->get('gas_bill');
            $member->electric_per_unit = $request->get('electric_per_unit');
            $member->description = $request->get('description');
            $member->save();
            $message = "Member updated successfully";
        } else {
            $message = "Member not updated";
        }

        $response = [
            'massage' => $message,
            'token' => null,
            'data' => $member,
        ];

        return response($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = null;
        $member = Member::find($id);
        $accounts = Account::where('member_id', $member->id)->get();

        if ($member->delete() && $accounts->delete()){
            $message = "Deleted successfully";
        } else {
            $message = "Delete failed";
        }

        $response = [
            'massage' => $message,
            'token' => null,
            'data' => $member,
        ];
        return response($response, 200);
    }
}
