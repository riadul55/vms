<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;

class AuthController extends Controller
{
    public function register (Request $request) {
        $message = null;
        $user = null;
        $token = null;

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            $message = $validator->errors()->first();
        } else {
            $request['password']=Hash::make($request['password']);
            $user = User::create($request->toArray());
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $message = "Registered successfully";
        }

        $response = [
            'massage' => $message,
            'token' => $token,
            'data' => $user,
        ];
        return response($response, 200);
    }

    public function login (Request $request) {
        $message = null;
        $user = null;
        $token = null;
        $user = null;

        if (is_numeric($request->username)){
            $user = User::where('phone', $request->username)->first();
        } else if (filter_var($request->username, FILTER_VALIDATE_EMAIL)){
            $user = User::where('email', $request->username)->first();
        }
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $message = "Login successfully";
            } else {
                $message = "Password missmatch";
            }
        } else {
            $message = 'User does not exist';
        }

        $response = [
            'massage' => $message,
            'token' => $token,
            'data' => $user,
        ];
        return response($response, 200);
    }

    public function show($id){
        $message = null;
        $token = null;
        $user = User::find($id);

        if ($user != null){
            $message = "Profile succeed";
        } else {
            $message = "Profile not found!";
        }

        $response = [
            'massage' => $message,
            'token' => $token,
            'data' => $user,
        ];
        return response($response, 200);
    }

    public function edit(Request $request, $id){
        $message = null;
        $token = null;

        $request = $request->all();
        $user = User::find($id);
        if ($user != null){
            if (isset($request->name)){
                $validator = Validator::make($request->name, [
                    'name' => 'required|string|max:255'
                ]);
                if ($validator->fails()) {
                    $message = $validator->errors()->first();
                } else {
                    $user->name = $request->name;
                    $user->save();
                    $message = "Name updated successfully";
                }
            }

            if (isset($request->email)){
                $validator = Validator::make($request->email, [
                    'email' => 'required|string|email|max:255|unique:users'
                ]);
                if ($validator->fails()) {
                    $message = $validator->errors()->first();
                } else {
                    $user->email = $request->email;
                    $user->save();
                    $message = "Email updated successfully";
                }
            }

            if (isset($request->phone)){
                $validator = Validator::make($request->phone, [
                    'phone' => 'required|string|max:255'
                ]);
                if ($validator->fails()) {
                    $message = $validator->errors()->first();
                } else {
                    $user->phone = $request->phone;
                    $user->save();
                    $message = "Phone updated successfully";
                }
            }

            if (isset($request->password)){
                $validator = Validator::make($request->password, [
                    'password' => 'required|string|min:6|confirmed'
                ]);
                if ($validator->fails()) {
                    $message = $validator->errors()->first();
                } else {
                    $user->password = $request->password;
                    $user->save();
                    $message = "Password updated successfully";
                }
            }
        } else {
            $message = "Profile not updated!";
        }

        $response = [
            'massage' => $message,
            'token' => $token,
            'data' => $user,
        ];
        return response($response, 200);
    }

    public function details() {
        return response()->json(['user' => auth()->user()], 200);
    }

    public function logout (Request $request) {
        $token = $request->user()->token();
        $token->revoke();
        $response = 'You have been succesfully logged out!';
        return response($response, 200);
    }
}
