<?php

namespace App\Http\Controllers;

use App\Account;
use App\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, Request $request)
    {
        $message = null;
        $accounts = Account::where('user_id', $id)
            ->whereMonth('created_at', $request->get('month'))
            ->whereYear('created_at', $request->get('year'))
            ->get();

        for ($i=0;$i<count($accounts);$i++){
            $member = Member::find($accounts[$i]->member_id);
            $accounts[$i]['name'] = $member->name;
        }

        if ($accounts != null){
            $message = "Accounts found";
        } else {
            $message = "Accounts not found";
        }

        $response = [
            'massage' => $message,
            'token' => null,
            'data' => $accounts,
        ];

        return response($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $message = null;
        $account = null;
        $token = null;

        $validator = Validator::make($request->all(), [
            'member_id' => 'required',
            'house_bill' => 'required',
            'unit_number' => 'required',
            'grand_total' => 'required',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();
        } else {
            $account = new Account();
            $account->user_id = $request->get('user_id');
            $account->member_id = $request->get('member_id');
            $account->house_bill = $request->get('house_bill');
            $account->water_bill = $request->get('water_bill');
            $account->gas_bill = $request->get('gas_bill');
            $account->electric_bill = $request->get('electric_bill');
            $account->prev_unit_number = $request->get('prev_unit_number');
            $account->unit_number = $request->get('unit_number');
            $account->grand_total = $request->get('grand_total');
            $account->due_bill = $request->get('due_bill');
            $account->save();
            $message = "Account created successfully";
        }

        $response = [
            'massage' => $message,
            'token' => $token,
            'data' => $account,
        ];
        return response($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = null;

        $date = Carbon::now();
        $prevAcc = Account::where('member_id', $id)->whereMonth('created_at', $date->subMonth())->first();
        $account = Account::where('member_id', $id)->whereMonth('created_at', $date->month)->first();
        $member = Member::find($id);
//        if ($request->get('month') && $request->get('sub_month')){
//            $prevAcc = Account::where('member_id', $id)->whereMonth('created_at', $request->get('sub_month'))->first();
//            $account = Account::where('member_id', $id)->whereMonth('created_at', $request->get('month'))->first();
//        } else {
//        }
//        if ($request->get('date')){
//            //year-month-date
//            $accounts = Account::where('member_id', $id)->whereDate('created_at', $request->get('date'))->get();
//        } elseif ($request->get('month')){
//            $accounts = Account::where('member_id', $id)->whereMonth('created_at', $request->get('month'))->get();
//        } elseif ($request->get('day')){
//            $accounts = Account::where('member_id', $id)->whereDay('created_at', $request->get('day'))->get();
//        }

        if ($member != null){
            $message = "Member found";
        } else {
            $message = "Member not found";
        }

        $response = [
            'massage' => $message,
            'token' => null,
            'preAcc' => isset($prevAcc) ? $prevAcc->prev_unit_number : null,
            'account' => isset($account) ? 1 : 0,
            'data' => $member,
        ];
        return response($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = null;
        $account = Account::find($id);

        if ($account != null){
            $message = "Account found";
        } else {
            $message = "Account not found";
        }

        $response = [
            'massage' => $message,
            'token' => null,
            'data' => $account,
        ];

        return response($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = null;
        $account = null;
        $token = null;

        $request = $request->all();
        $account = Account::find($id);
        if ($account != null){
            $date = Carbon::now();
            $prevAcc = Account::where('member_id', $account->member_id)->whereDate('created_at', $date->startOfMonth()->subMonth()->format('y-m-d'))->first();

            $account->member_id = $request->get('member_id');
            $account->house_bill = $request->get('house_bill');
            $account->water_bill = $request->get('water_bill');
            $account->gas_bill = $request->get('gas_bill');
            $account->electric_bill = $request->get('electric_bill');
            $account->prev_unit_number = $request->get('prev_unit_number');
            $account->unit_number = $request->get('unit_number');
            $account->grand_total = $request->get('grand_total');
            $account->due_bill = $request->get('due_bill');
            $account->save();
            $message = "Account updated successfully";
        } else {
            $message = "Account not updated";
        }

        $response = [
            'massage' => $message,
            'token' => $token,
            'data' => $account,
        ];
        return response($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = null;
        $account = Account::find($id);

        if ($account->delete()){
            $message = "Deleted successfully";
        } else {
            $message = "Delete failed";
        }

        $response = [
            'massage' => $message,
            'token' => null,
            'data' => $account,
        ];
        return response($response, 200);
    }
}
