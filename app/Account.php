<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
