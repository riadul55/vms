<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login')->name('login.api');
Route::post('/register', 'AuthController@register')->name('register.api');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('auth:api')->group(function () {

    Route::group(['prefix'=>'member','as'=>'member.'], function(){
        Route::get('all/{id}', 'MemberController@index')->name('all');
        Route::post('create', 'MemberController@create')->name('create');
        Route::get('show/{id}', 'MemberController@show')->name('show');
        Route::get('edit/{id}', 'MemberController@edit')->name('edit');
        Route::put('update/{id}', 'MemberController@update')->name('update');
        Route::delete('delete/{id}', 'MemberController@destroy')->name('delete');
    });

    Route::group(['prefix'=>'account','as'=>'account.'], function(){
        Route::get('all/{id}', 'AccountController@index')->name('all');
        Route::post('create', 'AccountController@create')->name('create');
        Route::get('show/{id}', 'AccountController@show')->name('show');
        Route::get('edit/{id}', 'AccountController@edit')->name('edit');
        Route::put('update/{id}', 'AccountController@update')->name('update');
        Route::delete('delete/{id}', 'AccountController@destroy')->name('delete');
    });

    Route::get('/logout', 'AuthController@logout')->name('logout');
});
