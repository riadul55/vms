<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('member_id');
            $table->double('house_bill');
            $table->double('water_bill')->nullable(true);
            $table->double('gas_bill')->nullable(true);
            $table->double('electric_bill')->nullable(true);
            $table->string('prev_unit_number')->nullable(true);
            $table->string('unit_number');
            $table->double('grand_total');
            $table->double('due_bill')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
